FROM nginx:1.14.0
ARG nginx_version=1.14.0

# FROM nginx:${nginx_version} as build

RUN apt-get update \
    && apt-get install -y --no-install-suggests \
       libluajit-5.1-dev libpam0g-dev zlib1g-dev libpcre3-dev \
       libexpat1-dev git curl build-essential libssl-dev openssl \
    && export NGINX_RAW_VERSION=$(echo $NGINX_VERSION | sed 's/-.*//g') \
    && curl -fSL https://nginx.org/download/nginx-$NGINX_RAW_VERSION.tar.gz -o nginx.tar.gz \
    && tar -zxC /usr/src -f nginx.tar.gz

ARG modules

COPY docker-nginx-sticky-module-ng /usr/src/nginx-${nginx_version}/nginx-sticky-module-ng/ 
COPY nginx_upstream_check_module /usr/src/nginx-${nginx_version}/nginx_upstream_check_module/

RUN export NGINX_RAW_VERSION=$(echo $NGINX_VERSION | sed 's/-.*//g') \
  && cd /usr/src/nginx-${NGINX_RAW_VERSION}/nginx-sticky-module-ng \
  && patch -p0 < /usr/src/nginx-${NGINX_RAW_VERSION}/nginx_upstream_check_module/nginx-sticky-module.patch \
  && cd /usr/src/nginx-${NGINX_RAW_VERSION} \
  && patch -p1 < /usr/src/nginx-${NGINX_RAW_VERSION}/nginx_upstream_check_module/check_1.14.0+.patch 



RUN export NGINX_RAW_VERSION=$(echo $NGINX_VERSION | sed 's/-.*//g') \
  && config_args="--prefix=/etc/nginx --sbin-path=/usr/sbin/nginx --modules-path=/usr/lib/nginx/modules --conf-path=/etc/nginx/nginx.conf --error-log-path=/var/log/nginx/error.log --http-log-path=/var/log/nginx/access.log --pid-path=/var/run/nginx.pid --lock-path=/var/run/nginx.lock --http-client-body-temp-path=/var/cache/nginx/client_temp --http-proxy-temp-path=/var/cache/nginx/proxy_temp --http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp --http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp --http-scgi-temp-path=/var/cache/nginx/scgi_temp --user=nginx --group=nginx --with-compat --with-file-aio --with-threads --with-http_addition_module --with-http_auth_request_module --with-http_dav_module --with-http_flv_module --with-http_gunzip_module --with-http_gzip_static_module --with-http_mp4_module --with-http_random_index_module --with-http_realip_module --with-http_secure_link_module --with-http_slice_module --with-http_ssl_module --with-http_stub_status_module --with-http_sub_module --with-http_v2_module --with-mail --with-mail_ssl_module --with-stream --with-stream_realip_module --with-stream_ssl_module --with-stream_ssl_preread_module --with-cc-opt='-g -O2 -fdebug-prefix-map=/data/builder/debuild/nginx-1.14.0/debian/debuild-base/nginx-1.14.0=. -specs=/usr/share/dpkg/no-pie-compile.specs -fstack-protector-strong -Wformat -Werror=format-security -Wp,-D_FORTIFY_SOURCE=2 -fPIC' --with-ld-opt='-specs=/usr/share/dpkg/no-pie-link.specs -Wl,-z,relro -Wl,-z,now -Wl,--as-needed -pie'" \
  && cd /usr/src/nginx-${NGINX_RAW_VERSION} \
  && eval ./configure ${config_args} \
      --add-module=/usr/src/nginx-${NGINX_RAW_VERSION}/nginx-sticky-module-ng \
      --add-module=/usr/src/nginx-${NGINX_RAW_VERSION}/nginx_upstream_check_module \
  && make \
  && make install

EXPOSE 80 443

