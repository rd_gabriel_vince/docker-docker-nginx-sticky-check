# Build custom docker nginx with sticky module and upstream check

based on 

 - https://github.com/tsuru/docker-nginx-with-modules.git
 - https://github.com/yaoweibin/nginx_upstream_check_module
 - https://bitbucket.org/nginx-goodies/nginx-sticky-module-ng/

sticky module fixed

 - https://bitbucket.org/rd_gabriel_vince/docker-nginx-sticky-module-ng

Example of the upstream with check

```
upstream nodes {

  server node1:80;
  server node2:80;

  check interval=2000 rise=1 fall=2 timeout=1000 type=http;
  check_http_send "HEAD / HTTP/1.0\r\n\r\n";
  check_http_expect_alive http_2xx http_3xx;

  sticky expires=4h;
}


server {
    listen       80;
    server_name  localhost;

    #access_log  /var/log/nginx/host.access.log  main;

    location / {
       proxy_pass http://nodes;
    }
}
```
